package cn.chowa.ejyy.user;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.utils.AuthUtil;
import cn.chowa.ejyy.dao.PropertyCompanyAuthRepository;
import cn.chowa.ejyy.dao.UserQuery;
import cn.chowa.ejyy.model.entity.PropertyCompanyAuth;
import cn.dev33.satoken.util.SaResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName logout
 * @Description TODO
 * @Author ironman
 * @Date 14:53 2022/8/12
 */
@Slf4j
@RestController
@RequestMapping("/pc/user")
public class logout {

    @Autowired
    private UserQuery userQuery;
    @Autowired
    private PropertyCompanyAuthRepository companyAuthRepository;

    /**
     * 退出登录
     */
    @GetMapping("/logout")
    public SaResult logout() {
        String userName = AuthUtil.getUserId();
        ObjData userInfo = userQuery.getUserInfo(Constants.status.FALSE, userName);
        //清空token
        PropertyCompanyAuth companyAuth = companyAuthRepository.findById(userInfo.getLong("id")).get();
        companyAuth.setToken(null);
        companyAuthRepository.save(companyAuth);
        //测试返回自定义结果
        SaResult res = new SaResult();
        res.setCode(Constants.code.SUCCESS);
        res.setMsg("账号已退出");
        return res;
    }
}
