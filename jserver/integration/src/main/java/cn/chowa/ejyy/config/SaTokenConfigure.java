package cn.chowa.ejyy.config;

import cn.chowa.ejyy.service.property_company;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.dev33.satoken.context.model.SaRequest;
import cn.dev33.satoken.interceptor.SaAnnotationInterceptor;
import cn.dev33.satoken.interceptor.SaRouteInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {

    @Autowired
    private property_company propertyCompany;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册注解拦截器
        registry.addInterceptor(new SaAnnotationInterceptor()).addPathPatterns("/**");
        // 注册路由拦截器，自定义认证规则
        registry.addInterceptor(new SaRouteInterceptor((req, res, handler) -> {
            log.info("resource role check,path:{}", req.getRequestPath());
            //检查是否登录
            StpUtil.checkLogin();
            if (handler instanceof HandlerMethod) {
//                checkCommunityAccess(req, (HandlerMethod) handler);
            }

        })).addPathPatterns("/**")
                .excludePathPatterns(
                        "/pc/user/account_login",
                        "/pc/user/logout",
                        "/pc/user/captcha",
                        "/pc/user/state",
                        "/mp/user/login"
                );
    }

    /**
     * 校验小区访问权限
     */
    private void checkCommunityAccess(SaRequest req, HandlerMethod method) {
        VerifyCommunity verify = method.getMethodAnnotation(VerifyCommunity.class);
        //权限校验访问权限
        if (verify != null && verify.value()) {
            boolean pass = false;
            try {
                String strUid = req.getParam("id");
                String strCommId = req.getParam("community_id");
                pass = propertyCompany.verifyCommunity(
                        Long.parseLong(strUid),
                        Long.parseLong(strCommId)
                );
            } catch (Throwable e) {
                log.error("checkCommunityAccess error", e);
            }
            if (!pass) {
                throw new CodeException(Constants.code.QUERY_ILLEFAL, "操作非法");
            }
        }
    }
}
