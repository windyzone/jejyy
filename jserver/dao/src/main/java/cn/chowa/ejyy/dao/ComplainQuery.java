package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

@JqlQuery
public interface ComplainQuery {

    List<ObjData> getComplains(Integer type, Integer category, long community_id, String refer, Integer step, int page_size, int page_num);

    long getComplainsCount(Integer type, Integer category, long community_id, String refer, Integer step);

}
