package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

@JqlQuery
public interface EpidemicQuery {

    List<ObjData> getBuildingEpidemics(int tour_code, int return_hometown, long community_id, int page_size, int page_num);

    long getBuildingEpidemicsCount(int tour_code, int return_hometown, long community_id);

    ObjData getEpidemicDetail(long id, long community_id);

}
