package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;
import lombok.Data;

import java.util.List;

@JqlQuery
public interface CommunityQuery {

    List<ObjData> getCommunitySettingInfo(long userId);

    ObjData getDepartmentJob(long userId);

}
